FROM gricad-registry.univ-grenoble-alpes.fr/osug/dc/shiny-base:4.3.2

USER root
RUN apt-get update && apt-get install --no-install-recommends -y \
    libfontconfig1-dev \
    libfribidi-dev \
    libgdal-dev \
    libharfbuzz-dev \
    libsodium-dev \
    libudunits2-dev \
    && rm -rf /var/lib/apt/lists/*
USER shiny

# Install dependencies
RUN install.r \
      shiny \
      tidyverse \
      sf \
      raster \
      mapview \
      data.tables \
      leaflet \
      && rm -rf /tmp/downloaded_packages

# Copy app files
COPY --chown=shiny app.R cbna_points.csv data_label.csv freqR.csv merge_spname.csv species_name.csv Trend.csv /srv/shiny/
